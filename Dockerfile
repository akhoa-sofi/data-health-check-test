FROM python:3.8-alpine3.11

RUN pip install -r requirements.txt

COPY job.py job.py

CMD ["python", "job.py"]