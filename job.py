import os

from data_health_check import DataHealthClient

DD_AGENT_HOST = os.getenv('DD_AGENT_HOST', 'localhost')
print(DD_AGENT_HOST)

cli = DataHealthClient('andy-test', 'andy-test-service', 'andy-test-pipeline', 
                        statsd_host=DD_AGENT_HOST)
cli.emit_gauge("test_gauge", 100, 't', 't')
cli.report_error('test_error')
cli.report_files_count()
cli.report_file_size(128, 'test_bucket', 'test_file')
cli.report_records(199, 'test_bucket', 'test_file')
cli.report_columns(30, 'test_bucket', 'test_file')
cli.report_elapsed_time(120, 'test_bucket', 'test_file')